FROM golang:1.21

ADD common .
ADD server .
WORKDIR /server
EXPOSE 8080
RUN go build .
CMD ["./server"]