package main

import (
	"context"
	"devops/server/internal/adapters/controllers/http"
	"devops/server/internal/repository/bot_admin_repo"
	"devops/server/internal/repository/bots_repo"
	"devops/server/internal/usecase/bot_admin"
	"devops/server/internal/usecase/bots"
	"devops/server/internal/usecase/tg_worker"
	jwtauth "github.com/go-chi/jwtauth/v5"
	pgx "github.com/jackc/pgx/v5"
	"github.com/lestrrat-go/jwx/v2/jwt"
	"log/slog"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())
	exit := make(chan os.Signal, 1) // we need to reserve to buffer size 1, so the notifier are not blocked
	signal.Notify(exit, os.Interrupt, syscall.SIGTERM)
	wg := &sync.WaitGroup{}
	logger := slog.New(
		slog.NewJSONHandler(os.Stdout, nil),
	)
	slog.SetDefault(logger)

	dbUrl := os.Getenv("DATABASE_URL")
	if dbUrl == "" {
		dbUrl = "postgres://postgres:password@localhost:5432/bots_platform"
	}
	conn, err := pgx.Connect(ctx, dbUrl)
	if err != nil {
		logger.Error("unable to connect to database", "err", err)
		os.Exit(1)
	}

	err = conn.Ping(ctx)
	if err != nil {
		logger.Error("error ping postgres", "err", err)
		os.Exit(1)
	}
	botAdminRepo := bot_admin_repo.New(conn)
	jwtSecret := os.Getenv("JWT_SECRET")
	if jwtSecret == "" {
		jwtSecret = "123456"
	}
	tokenAuth := jwtauth.New("HS256", []byte(jwtSecret), nil, jwt.WithAcceptableSkew(30*time.Second))
	authUseCase := bot_admin.New(logger, conn, botAdminRepo, tokenAuth)
	botsRepo := bots_repo.New(conn)
	tgWorker := tg_worker.New(ctx, logger, wg, botsRepo)
	botsUseCase := bots.New(logger, botsRepo, tgWorker)
	http.NewServer(ctx, logger, wg, authUseCase, tokenAuth, botsUseCase)

	select {
	case <-exit:
		cancel()
		wg.Wait()
		conn.Close(context.Background())
		logger.Info("gracefully shut down")
	}
}
