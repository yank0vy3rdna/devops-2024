package tg_worker

import (
	"context"
	"devops/server/internal/repository/bots_repo"
	"fmt"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/go-telegram/bot"
	"github.com/go-telegram/bot/models"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgtype"
	"log/slog"
	"sync"
	"sync/atomic"
)

type worker struct {
	inShutdown    atomic.Bool // true when worker is in shutdown
	mu            sync.Mutex
	botsRepo      *bots_repo.Queries
	logger        *slog.Logger
	chByBotId     map[uuid.UUID]chan struct{}
	botsWaitGroup sync.WaitGroup
	ctx           context.Context
}

func (w *worker) shuttingDown() bool {
	return w.inShutdown.Load()
}
func New(ctx context.Context, logger *slog.Logger, wg *sync.WaitGroup, botsRepo *bots_repo.Queries) *worker {
	w := &worker{botsRepo: botsRepo, logger: logger, chByBotId: make(map[uuid.UUID]chan struct{}), ctx: ctx}
	go w.run(ctx, wg)
	return w
}
func (w *worker) stopTheBot(botId uuid.UUID) {
	w.mu.Lock()
	defer w.mu.Unlock()
	if ch, ok := w.chByBotId[botId]; ok {
		close(ch)
		delete(w.chByBotId, botId)
	}
}

func (w *worker) processBot(ctx context.Context, ch <-chan struct{}, b bots_repo.Bot) {
	w.botsWaitGroup.Add(1)
	defer w.botsWaitGroup.Done()
	defer w.stopTheBot(b.ID.Bytes)

	opts := []bot.Option{
		bot.WithDefaultHandler(func(ctx context.Context, botI *bot.Bot, update *models.Update) {
			botFromDb, err := w.botsRepo.GetBotById(ctx, bots_repo.GetBotByIdParams{
				OwnerID: b.OwnerID,
				ID:      b.ID,
			})
			if err != nil {
				w.logger.Error("error fetching bot from db", "err", err, "botId", b.ID)
				return
			}
			if !botFromDb.Enabled {
				w.logger.Warn("not enabled bot was running, shutting down", "botId", b.ID)
				w.stopTheBot(b.ID.Bytes)
				return
			}
			if update.Message != nil { // If we got a message
				w.logger.Debug("incoming message", "from", update.Message.From.Username, "text", update.Message.Text)

				msg := tgbotapi.NewMessage(update.Message.Chat.ID, botFromDb.HelloMessage)
				msg.ReplyToMessageID = update.Message.ID

				_, err = botI.SendMessage(ctx, &bot.SendMessageParams{
					ChatID: update.Message.Chat.ID,
					Text:   botFromDb.HelloMessage,
				})
				if err != nil {
					w.logger.Error("error sending message", "err", err, "botId", b.ID)
					return
				}
			}
		}),
	}

	botI, err := bot.New(b.Token, opts...)
	if err != nil {
		w.logger.Error("error init bot", "botId", b.ID, "err", err)
		return
	}
	ctx, cancel := context.WithCancel(ctx)
	w.logger.Info("bot starting to receive updates...", "id", b.ID)
	go func() {
		select {
		case <-ch:
			w.logger.Info("bot is shutting down", "id", b.ID)
			cancel()
		}
	}()
	botI.Start(ctx)
	w.logger.Info("bot finished to receive updates", "id", b.ID)
}
func (w *worker) init(ctx context.Context) {
	bots, err := w.botsRepo.GetAllEnabledBots(ctx)
	if err != nil {
		w.logger.Error("error fetching enabled bots", "err", err)
		return
	}
	w.mu.Lock()
	defer w.mu.Unlock()
	for _, bot := range bots {
		ch := make(chan struct{})
		w.chByBotId[bot.ID.Bytes] = ch
		go w.processBot(ctx, ch, bot)
	}
}
func (w *worker) DisableBot(botId uuid.UUID) {
	w.stopTheBot(botId)
}
func (w *worker) EnableBot(ctx context.Context, botId uuid.UUID, ownerId uuid.UUID) error {
	bot, err := w.botsRepo.GetBotById(ctx, bots_repo.GetBotByIdParams{
		OwnerID: pgtype.UUID{Bytes: ownerId, Valid: true},
		ID:      pgtype.UUID{Bytes: botId, Valid: true},
	})
	if err != nil {
		return fmt.Errorf("error fetching bot from db: %w", err)
	}

	w.mu.Lock()
	defer w.mu.Unlock()
	if _, ok := w.chByBotId[botId]; ok {
		w.logger.Warn("bot is already running", "id", botId)
		return nil
	}

	ch := make(chan struct{})
	w.chByBotId[bot.ID.Bytes] = ch
	go w.processBot(w.ctx, ch, bot)
	return nil
}
func (w *worker) close() {
	w.mu.Lock()
	for _, c := range w.chByBotId {
		close(c)
	}
	w.chByBotId = make(map[uuid.UUID]chan struct{})
	w.mu.Unlock()
	w.botsWaitGroup.Wait()
}
func (w *worker) run(ctx context.Context, wg *sync.WaitGroup) {
	wg.Add(1)

	w.init(ctx)

	select {
	case <-ctx.Done():
		w.close()
		wg.Done()
	}
}
