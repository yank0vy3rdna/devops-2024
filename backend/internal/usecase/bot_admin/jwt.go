package bot_admin

import (
	"fmt"
	"github.com/golang-jwt/jwt/v5"
	"github.com/google/uuid"
	"time"
)

func generateToken(userId uuid.UUID, jwtSecretKey string) (string, error) {
	payload := jwt.MapClaims{
		"sub": userId,
		"exp": time.Now().Add(time.Hour * 3).Unix(),
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, payload)

	t, err := token.SignedString([]byte(jwtSecretKey))
	if err != nil {
		return "", fmt.Errorf("JWT token signing: %w", err)
	}
	return t, nil
}
