package bot_admin

import (
	"context"
	"devops/server/internal/repository/bot_admin_repo"
	"errors"
	"fmt"
	"github.com/go-chi/jwtauth/v5"
	"github.com/jackc/pgx/v5"
	"log/slog"
	"time"
)

type useCase struct {
	botAdminRepo *bot_admin_repo.Queries
	conn         *pgx.Conn
	logger       *slog.Logger
	tokenAuth    *jwtauth.JWTAuth
}

func (u useCase) Register(ctx context.Context, login, password string) (bool, error) {
	tx, err := u.conn.Begin(ctx)
	if err != nil {
		return false, fmt.Errorf("unable to begin tx: %w", err)
	}
	repo := u.botAdminRepo.WithTx(tx)
	exists, err := repo.IsUserExists(ctx, login)
	if err != nil {
		tx.Rollback(ctx)
		return false, fmt.Errorf("unable to check if user exists: %w", err)
	}
	if exists {
		tx.Rollback(ctx)
		return false, nil
	}
	bytes, err := hashPassword(password)
	if err != nil {
		tx.Rollback(ctx)
		return false, fmt.Errorf("error while hashing password: %w", err)
	}
	_, err = repo.RegisterBotAdmin(ctx, bot_admin_repo.RegisterBotAdminParams{
		Login:    login,
		Password: bytes,
	})
	if err != nil {
		tx.Rollback(ctx)
		return false, fmt.Errorf("error while inserting new bot admin: %w", err)
	}

	tx.Commit(ctx)

	return true, nil
}
func (u useCase) Auth(ctx context.Context, login, password string) (string, error) {
	user, err := u.botAdminRepo.GetUserByLogin(ctx, login)
	if errors.Is(err, pgx.ErrNoRows) {
		return "", fmt.Errorf("user not found: %w", err)
	}
	if err != nil {
		return "", fmt.Errorf("err fetch user from db: %w", err)
	}
	passwordCorrect := checkPasswordHash(password, user.Password)
	if !passwordCorrect {
		return "", fmt.Errorf("wrong password")
	}
	_, token, err := u.tokenAuth.Encode(map[string]interface{}{
		"userId": user.ID,
		"exp":    time.Now().Add(time.Hour),
	})
	if err != nil {
		return "", err
	}
	if err != nil {
		return "", fmt.Errorf("error generating jwt: %w", err)
	}
	return token, nil
}

func New(logger *slog.Logger, conn *pgx.Conn, botAdminRepo *bot_admin_repo.Queries, tokenAuth *jwtauth.JWTAuth) *useCase {
	return &useCase{logger: logger, botAdminRepo: botAdminRepo, conn: conn, tokenAuth: tokenAuth}
}
