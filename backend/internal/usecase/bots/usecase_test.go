package bots

import (
	"devops/server/internal/domain"
	"devops/server/internal/repository/bots_repo"
	"reflect"
	"testing"
)

func Test_convertBotToDomain(t *testing.T) {
	type args struct {
		bot bots_repo.Bot
	}
	tests := []struct {
		name string
		args args
		want domain.Bot
	}{
		{
			"test",
			args{},
			domain.Bot{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := convertBotToDomain(tt.args.bot); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("convertBotToDomain() = %v, want %v", got, tt.want)
			}
		})
	}
}
