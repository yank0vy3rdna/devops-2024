package bots

import (
	"context"
	"devops/server/internal/domain"
	"devops/server/internal/repository/bots_repo"
	"fmt"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgtype"
	"log/slog"
)

type useCase struct {
	logger   *slog.Logger
	botsRepo *bots_repo.Queries
	tgWorker TgWorker
}

func convertBotToDomain(bot bots_repo.Bot) domain.Bot {
	return domain.Bot{
		Id:      bot.ID.Bytes,
		OwnerId: bot.OwnerID.Bytes,
		Token:   bot.Token,
		Hello:   bot.HelloMessage,
		Enabled: bot.Enabled,
	}
}
func convertBotsToDomain(bots []bots_repo.Bot) []domain.Bot {
	res := make([]domain.Bot, len(bots))
	for i, bot := range bots {
		res[i] = convertBotToDomain(bot)
	}
	return res
}
func (u useCase) GetBotsByUserID(ctx context.Context, userID uuid.UUID) ([]domain.Bot, error) {
	bots, err := u.botsRepo.GetBotsByOwnerID(ctx, pgtype.UUID{Bytes: userID, Valid: true})
	if err != nil {
		return nil, fmt.Errorf("error fetching bots from db: %w", err)
	}
	return convertBotsToDomain(bots), nil
}
func (u useCase) GetBotByID(ctx context.Context, userID uuid.UUID, botID uuid.UUID) (domain.Bot, error) {
	bot, err := u.botsRepo.GetBotById(ctx, bots_repo.GetBotByIdParams{
		OwnerID: pgtype.UUID{Bytes: userID, Valid: true},
		ID:      pgtype.UUID{Bytes: botID, Valid: true},
	})
	if err != nil {
		return domain.Bot{}, fmt.Errorf("error fetching bots from db: %w", err)
	}
	return convertBotToDomain(bot), nil
}
func (u useCase) CreateBot(ctx context.Context, userID uuid.UUID, token string, message string) (domain.Bot, error) {
	bot, err := u.botsRepo.CreateBot(ctx, bots_repo.CreateBotParams{
		OwnerID:      pgtype.UUID{Bytes: userID, Valid: true},
		Token:        token,
		HelloMessage: message,
	})
	if err != nil {
		return domain.Bot{}, fmt.Errorf("error create bot: %w", err)
	}
	return convertBotToDomain(bot), nil
}

func (u useCase) DeleteBot(ctx context.Context, userID uuid.UUID, botID uuid.UUID) error {
	u.tgWorker.DisableBot(botID)
	err := u.botsRepo.DeleteBot(ctx, bots_repo.DeleteBotParams{
		OwnerID: pgtype.UUID{Bytes: userID, Valid: true},
		ID:      pgtype.UUID{Bytes: botID, Valid: true},
	})
	if err != nil {
		return fmt.Errorf("error delete bot: %w", err)
	}
	return nil
}

func (u useCase) UpdateBot(ctx context.Context, userID uuid.UUID, botID uuid.UUID, helloMessage string) error {
	err := u.botsRepo.UpdateBotMessage(ctx, bots_repo.UpdateBotMessageParams{
		OwnerID:      pgtype.UUID{Bytes: userID, Valid: true},
		ID:           pgtype.UUID{Bytes: botID, Valid: true},
		HelloMessage: helloMessage,
	})
	if err != nil {
		return fmt.Errorf("error update bot: %w", err)
	}
	return nil
}

func (u useCase) ChangeAdministrativeState(ctx context.Context, userID uuid.UUID, botID uuid.UUID) error {
	isEnabled, err := u.botsRepo.ChangeBotAdministrativeState(ctx, bots_repo.ChangeBotAdministrativeStateParams{
		OwnerID: pgtype.UUID{Bytes: userID, Valid: true},
		ID:      pgtype.UUID{Bytes: botID, Valid: true},
	})
	if err != nil {
		return fmt.Errorf("error change administrative state for bot: %w", err)
	}
	if isEnabled {
		err = u.tgWorker.EnableBot(ctx, botID, userID)
		if err != nil {
			return fmt.Errorf("error enabling bot: %w", err)
		}
	} else {
		u.tgWorker.DisableBot(botID)
	}
	return nil
}

type TgWorker interface {
	EnableBot(ctx context.Context, botId uuid.UUID, ownerId uuid.UUID) error
	DisableBot(botId uuid.UUID)
}

func New(logger *slog.Logger, botsRepo *bots_repo.Queries, tgWorker TgWorker) *useCase {
	return &useCase{logger: logger, botsRepo: botsRepo, tgWorker: tgWorker}
}
