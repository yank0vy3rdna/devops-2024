-- name: RegisterBotAdmin :one
insert into bot_admin (login, password)
VALUES ($1, $2)
RETURNING *;

-- name: IsUserExists :one
select count(*) > 0
from bot_admin
where login = $1;

-- name: GetUserByLogin :one
select *
from bot_admin
where login = $1;