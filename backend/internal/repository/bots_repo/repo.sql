-- name: GetBotsByOwnerID :many
select *
from bots
where owner_id = $1;

-- name: CreateBot :one
insert into bots (owner_id, token, hello_message)
VALUES ($1, $2, $3)
RETURNING *;
-- name: GetBotById :one
select *
from bots
where owner_id = $1
  and id = $2;
-- name: UpdateBotMessage :exec
update bots
set hello_message=$1
where id = $2
  and owner_id = $3;

-- name: DeleteBot :exec
delete
from bots
where id = $1
  and owner_id = $2;

-- name: ChangeBotAdministrativeState :one
update bots
set enabled=NOT enabled
where id = $1
  and owner_id = $2
returning enabled;

-- name: GetAllEnabledBots :many
select *
from bots
where enabled;