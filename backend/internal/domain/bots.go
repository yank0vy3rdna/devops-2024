package domain

import "github.com/google/uuid"

type Bot struct {
	Id      uuid.UUID
	OwnerId uuid.UUID
	Token   string
	Hello   string
	Enabled bool
}
