package utils

import (
	"github.com/go-chi/jwtauth/v5"
	"github.com/google/uuid"
	"net/http"
)

func UserIdFromRequest(r *http.Request) (uuid.UUID, error) {
	_, claims, _ := jwtauth.FromContext(r.Context())

	userIdStr := claims["userId"].(string)
	parse, err := uuid.Parse(userIdStr)
	if err != nil {
		return [16]byte{}, err
	}
	return parse, nil
}
