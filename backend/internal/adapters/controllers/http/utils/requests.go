package utils

import (
	"encoding/json"
	"io"
)

func UnmarshalRequest[T any](body io.Reader, res *T) error {
	bytes, err := io.ReadAll(body)
	if err != nil {
		return err
	}
	err = json.Unmarshal(bytes, res)
	if err != nil {
		return err
	}
	return nil
}

func MarshalResponse[T any](body io.Writer, res T) error {
	bytes, err := json.Marshal(res)
	if err != nil {
		return err
	}
	_, err = body.Write(bytes)
	if err != nil {
		return err
	}
	return nil
}
