package bots

import (
	"devops/server/internal/adapters/controllers/http/utils"
	"devops/server/internal/domain"
	"github.com/go-chi/chi/v5"
	"github.com/google/uuid"
	"net/http"
)

type getBotsResponse struct {
	Bots []Bot
}
type Bot struct {
	Id      string `json:"id"`
	OwnerId string `json:"ownerId"`
	Token   string `json:"token"`
	Hello   string `json:"hello"`
	Enabled bool   `json:"enabled"`
}

func domainBotsToResponse(in []domain.Bot) []Bot {
	res := make([]Bot, len(in))
	for i, bot := range in {
		res[i] = domainBotToResponse(bot)
	}
	return res
}

func domainBotToResponse(in domain.Bot) Bot {
	return Bot{
		Id:      in.Id.String(),
		OwnerId: in.OwnerId.String(),
		Token:   in.Token,
		Hello:   in.Hello,
		Enabled: in.Enabled,
	}
}

func (h botsHandler) HandleGetBots(w http.ResponseWriter, r *http.Request) {
	userID, err := utils.UserIdFromRequest(r)
	if err != nil {
		h.logger.Error("error get user id from request", "err", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	bots, err := h.botsUseCase.GetBotsByUserID(r.Context(), userID)
	if err != nil {
		h.logger.Error("error get bots", "err", err, "userID", userID)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	err = utils.MarshalResponse(w, getBotsResponse{Bots: domainBotsToResponse(bots)})
	if err != nil {
		h.logger.Error("error marshal response", "err", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (h botsHandler) HandleGetBotById(w http.ResponseWriter, r *http.Request) {
	botID, err := uuid.Parse(chi.URLParam(r, "id"))
	if err != nil {
		h.logger.Error("error parsing bot id", "err", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	userID, err := utils.UserIdFromRequest(r)
	if err != nil {
		h.logger.Error("error get user id from request", "err", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	bot, err := h.botsUseCase.GetBotByID(r.Context(), userID, botID)
	if err != nil {
		h.logger.Error("error get bots", "err", err, "userID", userID)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	err = utils.MarshalResponse(w, domainBotToResponse(bot))
	if err != nil {
		h.logger.Error("error marshal response", "err", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

type createBotRequest struct {
	Token        string `json:"token"`
	HelloMessage string `json:"helloMessage"`
}

func (h botsHandler) HandleCreateBot(w http.ResponseWriter, r *http.Request) {
	var req createBotRequest
	err := utils.UnmarshalRequest(r.Body, &req)
	if err != nil {
		h.logger.Error("error unmarshal req", "err", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	userID, err := utils.UserIdFromRequest(r)
	if err != nil {
		h.logger.Error("error get user id from request", "err", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	_, err = h.botsUseCase.CreateBot(r.Context(), userID, req.Token, req.HelloMessage)
	if err != nil {
		h.logger.Error("error create bot", "err", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusCreated)
}
func (h botsHandler) HandleDeleteBot(w http.ResponseWriter, r *http.Request) {
	userID, err := utils.UserIdFromRequest(r)
	if err != nil {
		h.logger.Error("error get user id from request", "err", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	botID, err := uuid.Parse(chi.URLParam(r, "id"))
	if err != nil {
		h.logger.Error("error parsing bot id", "err", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	err = h.botsUseCase.DeleteBot(r.Context(), userID, botID)
	if err != nil {
		h.logger.Error("error delete bot", "err", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
}

type updateBotRequest struct {
	HelloMessage string `json:"helloMessage"`
}

func (h botsHandler) HandleUpdateBot(w http.ResponseWriter, r *http.Request) {
	botID, err := uuid.Parse(chi.URLParam(r, "id"))
	if err != nil {
		h.logger.Error("error parsing bot id", "err", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	var req updateBotRequest
	err = utils.UnmarshalRequest(r.Body, &req)
	if err != nil {
		h.logger.Error("error unmarshal req", "err", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	userID, err := utils.UserIdFromRequest(r)
	if err != nil {
		h.logger.Error("error get user id from request", "err", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	err = h.botsUseCase.UpdateBot(r.Context(), userID, botID, req.HelloMessage)
	if err != nil {
		h.logger.Error("error update bot", "err", err, "userID", userID, "botID", botID, "HelloMessage", req.HelloMessage)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
}
