package bots

import (
	"devops/server/internal/adapters/controllers/http/utils"
	"github.com/go-chi/chi/v5"
	"github.com/google/uuid"
	"net/http"
)

func (h botsHandler) HandleChangeAdministrativeState(w http.ResponseWriter, r *http.Request) {
	botID, err := uuid.Parse(chi.URLParam(r, "id"))
	if err != nil {
		h.logger.Error("error parsing bot id", "err", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	userID, err := utils.UserIdFromRequest(r)
	if err != nil {
		h.logger.Error("error get user id from request", "err", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	err = h.botsUseCase.ChangeAdministrativeState(r.Context(), userID, botID)
	if err != nil {
		h.logger.Error("error update bot", "err", err, "userID", userID, "botID", botID)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
}
