package bots

import (
	"context"
	"devops/server/internal/domain"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/jwtauth/v5"
	"github.com/google/uuid"
	"log/slog"
)

type BotsUseCase interface {
	GetBotsByUserID(context context.Context, userID uuid.UUID) ([]domain.Bot, error)
	GetBotByID(context context.Context, userID uuid.UUID, botID uuid.UUID) (domain.Bot, error)
	CreateBot(ctx context.Context, userID uuid.UUID, token string, message string) (domain.Bot, error)
	DeleteBot(ctx context.Context, userID uuid.UUID, botID uuid.UUID) error
	UpdateBot(ctx context.Context, userID uuid.UUID, botID uuid.UUID, helloMessage string) error
	ChangeAdministrativeState(ctx context.Context, userID uuid.UUID, botID uuid.UUID) error
}
type botsHandler struct {
	logger      *slog.Logger
	botsUseCase BotsUseCase
}

func Init(logger *slog.Logger, mux *chi.Mux, botsUseCase BotsUseCase, tokenAuth *jwtauth.JWTAuth) {
	handler := botsHandler{logger: logger, botsUseCase: botsUseCase}
	mux.Route("/bots", func(r chi.Router) {
		r.Use(jwtauth.Verifier(tokenAuth))
		r.Use(jwtauth.Authenticator(tokenAuth))
		r.Get("/", handler.HandleGetBots)
		r.Post("/", handler.HandleCreateBot)
		r.Delete("/{id}", handler.HandleDeleteBot)
		r.Get("/{id}", handler.HandleGetBotById)
		r.Patch("/{id}", handler.HandleUpdateBot)
		r.Post("/{id}/changeAdministrativeState", handler.HandleChangeAdministrativeState)
	})
}
