package handlers

import (
	"devops/server/internal/adapters/controllers/http/handlers/bot_admin"
	"devops/server/internal/adapters/controllers/http/handlers/bots"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/jwtauth/v5"
	"log/slog"
)

func Init(logger *slog.Logger, mux *chi.Mux, authUseCase bot_admin.AuthUseCase, tokenAuth *jwtauth.JWTAuth, botsUseCase bots.BotsUseCase) {
	bot_admin.Init(logger, mux, authUseCase)
	bots.Init(logger, mux, botsUseCase, tokenAuth)
}
