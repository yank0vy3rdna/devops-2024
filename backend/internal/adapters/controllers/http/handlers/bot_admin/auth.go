package bot_admin

import (
	"devops/server/internal/adapters/controllers/http/utils"
	"log/slog"
	"net/http"
)

type authHandler struct {
	logger      *slog.Logger
	authUseCase AuthUseCase
}
type registerRequestBody struct {
	Login    string `json:"username"`
	Password string `json:"password"`
}

func (h authHandler) HandleRegister(w http.ResponseWriter, r *http.Request) {
	var (
		req registerRequestBody
	)
	err := utils.UnmarshalRequest(r.Body, &req)
	if err != nil {
		h.logger.Error("error unmarshal req", "err", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	userCreated, err := h.authUseCase.Register(r.Context(), req.Login, req.Password)
	if err != nil {
		h.logger.Error("error register bot_admin_repo", "err", err, "req", req)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if !userCreated {
		w.WriteHeader(http.StatusUnprocessableEntity)
		return
	}
	w.WriteHeader(http.StatusCreated)
}

type authRequestBody struct {
	Login    string `json:"username"`
	Password string `json:"password"`
}
type authResponseBody struct {
	Token string `json:"token"`
}

func (h authHandler) HandleAuth(w http.ResponseWriter, r *http.Request) {
	var (
		req authRequestBody
	)
	err := utils.UnmarshalRequest(r.Body, &req)
	if err != nil {
		h.logger.Error("error unmarshal req", "err", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	token, err := h.authUseCase.Auth(r.Context(), req.Login, req.Password)
	if err != nil {
		h.logger.Error("error auth bot_admin", "err", err, "req", req)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	err = utils.MarshalResponse(w, authResponseBody{Token: token})
	if err != nil {
		h.logger.Error("error marshalling response", "err", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}
