package bot_admin

import (
	"context"
	"github.com/go-chi/chi/v5"
	"log/slog"
)

type AuthUseCase interface {
	Register(ctx context.Context, login, password string) (bool, error)
	Auth(ctx context.Context, login, password string) (string, error)
}

func Init(logger *slog.Logger, mux *chi.Mux, authUseCase AuthUseCase) {
	handler := authHandler{logger: logger, authUseCase: authUseCase}
	mux.Post("/register", handler.HandleRegister)
	mux.Post("/auth", handler.HandleAuth)
}
