package http

import (
	"context"
	"devops/server/internal/adapters/controllers/http/handlers"
	"devops/server/internal/adapters/controllers/http/handlers/bot_admin"
	"devops/server/internal/adapters/controllers/http/handlers/bots"
	"errors"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/cors"
	"github.com/go-chi/jwtauth/v5"
	"log/slog"
	"net/http"
	"sync"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
)

type server struct {
	logger      *slog.Logger
	authUseCase bot_admin.AuthUseCase
	tokenAuth   *jwtauth.JWTAuth
	botsUseCase bots.BotsUseCase
}

func NewServer(ctx context.Context, logger *slog.Logger, wg *sync.WaitGroup, authUseCase bot_admin.AuthUseCase, tokenAuth *jwtauth.JWTAuth, botsUseCase bots.BotsUseCase) *server {
	r := &server{logger: logger, authUseCase: authUseCase, tokenAuth: tokenAuth, botsUseCase: botsUseCase}
	go r.run(ctx, wg)
	return r
}

type reqLogger struct {
	logger *slog.Logger
}

func (r reqLogger) Print(v ...interface{}) {
	r.logger.Info("incoming request", "info", v[0].(string))
}

func (s *server) run(ctx context.Context, wg *sync.WaitGroup) {
	wg.Add(1)
	r := chi.NewRouter()
	middleware.DefaultLogger = middleware.RequestLogger(&middleware.DefaultLogFormatter{Logger: reqLogger{s.logger}, NoColor: true})
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	r.Use(render.SetContentType(render.ContentTypeJSON))
	r.Use(cors.Handler(cors.Options{
		// AllowedOrigins:   []string{"https://foo.com"}, // Use this to allow specific origin hosts
		AllowedOrigins: []string{"https://*", "http://*"},
		// AllowOriginFunc:  func(r *http.Request, origin string) bool { return true },
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS", "PATCH"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: false,
		MaxAge:           300, // Maximum value not ignored by any of major browsers
	}))

	handlers.Init(s.logger, r, s.authUseCase, s.tokenAuth, s.botsUseCase)

	httpServer := &http.Server{Addr: ":3001", Handler: r}
	go func() {
		err := httpServer.ListenAndServe()
		if errors.Is(err, http.ErrServerClosed) {
			return
		}
		if err != nil {
			s.logger.Error("error while listening: %w", err)

			return
		}
	}()
	select {
	case <-ctx.Done():
		ctx, cancel := context.WithTimeout(context.Background(), time.Second)
		defer cancel()
		err := httpServer.Shutdown(ctx)
		if err != nil {
			s.logger.Error("error while http server shutdown: %w", err)

			return
		}
		wg.Done()
	}
}
