"use client"
import FormHeader from "@/app/components/FormHeader";
import Alert from "@/app/components/Alert";
import Username from "@/app/components/Username";
import Password from "@/app/components/Password";
import SubmitButton from "@/app/components/SubmitButton";
import {FormEvent, useState} from "react";
import axios from "axios";
import useAuth from "@/app/hooks/auth";

export default function Page() {
    const [isAlertIsVisible, setIsAlertIsVisible] = useState(false)
    const [, isAuthenticated, setToken] = useAuth()

    async function onSubmit(event: FormEvent<HTMLFormElement>) {
        event.preventDefault()
        type AuthResponse = {
            token: string
        }
        const response = await axios.post<AuthResponse>("http://localhost:3001/auth", event.currentTarget, {
            headers: {
                'Content-Type': 'application/json'
            },
            validateStatus: null,
        })

        if (response.status == 200) {
            setIsAlertIsVisible(false)

            setToken(response.data.token)
            window.location.replace("/")
        } else {
            setIsAlertIsVisible(true)
            alert("Internal server error")
        }
    }

    if (isAuthenticated) {
        return <div>You&apos;re already logged in</div>
    }

    return (
        <FormHeader text={"Sign In"}>

            <div className="mt-10 sm:mx-auto sm:w-full sm:max-w-sm">

                <form className="space-y-6" onSubmit={onSubmit}>
                    {isAlertIsVisible ? <Alert text={"Invalid login or password"}
                                               onClick={() => setIsAlertIsVisible(false)}/> : <></>}

                    <Username/>

                    <Password/>

                    <SubmitButton text={"Sign in"}/>
                </form>
            </div>
        </FormHeader>
    );
}
