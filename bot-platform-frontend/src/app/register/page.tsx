'use client'
import {FormEvent, useState} from "react";
import axios from "axios";
import {useRouter} from "next/navigation";
import FormHeader from "@/app/components/FormHeader";
import Alert from "@/app/components/Alert";
import Password from "@/app/components/Password";
import Username from "@/app/components/Username";
import SubmitButton from "@/app/components/SubmitButton";
import useAuth from "@/app/hooks/auth";

export default function Page() {
    const [isAlertIsVisible, setIsAlertIsVisible] = useState(false)
    const router = useRouter()
    const [, isAuthenticated] = useAuth()

    async function onSubmit(event: FormEvent<HTMLFormElement>) {
        event.preventDefault()

        const response = await axios.post("http://localhost:3001/register", event.currentTarget, {
            headers: {
                'Content-Type': 'application/json'
            },
            validateStatus: null,
        })

        if (response.status == 422) {
            setIsAlertIsVisible(true)
        } else if (response.status == 201) {
            setIsAlertIsVisible(false)
            router.push("/login")
        } else {
            alert("Internal server error")
        }
    }

    if (isAuthenticated) {
        return <div>You&apos;re already logged in</div>
    }
    return (
        <FormHeader text={"Sign Up"}>

            <div className="mt-10 sm:mx-auto sm:w-full sm:max-w-sm">

                <form className="space-y-6" onSubmit={onSubmit}>
                    {isAlertIsVisible ? <Alert text={"User already exists"}
                                               onClick={() => setIsAlertIsVisible(false)}/> : <></>}

                    <Username/>

                    <Password/>

                    <SubmitButton text={"Sign up"}/>
                </form>
            </div>
        </FormHeader>
    );
}
