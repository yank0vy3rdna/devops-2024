"use client"
import {Inter} from "next/font/google";
import "./globals.css";
import React, {Suspense} from "react";
import Link from "next/link";
import useAuth from "@/app/hooks/auth";

const inter = Inter({subsets: ["latin"]});

export default function RootLayout({
                                       children,
                                   }: Readonly<{
    children: React.ReactNode;
}>) {
    const [, isAuthenticated, , logOut] = useAuth()
    return (
        <html lang="en">
        <body className={inter.className}>
        <Suspense>
            <header className="text-gray-600 body-font">
                <div className="container mx-auto flex flex-wrap p-5 flex-col md:flex-row items-center">
                    <a className="flex title-font font-medium items-center text-gray-900 mb-4 md:mb-0">
                        <span className="ml-3 text-xl">Bots Platform</span>
                    </a>
                    <nav
                        className="md:mr-auto md:ml-4 md:py-1 md:pl-4 md:border-l md:border-gray-400	flex flex-wrap items-center text-base justify-center">
                        <Link href={"/"} className="mr-5 hover:text-gray-900">Что это?</Link>
                        {isAuthenticated ?
                            <Link href={"/bots"} className="mr-5  hover:text-gray-900">Мои боты</Link> : <></>}
                    </nav>
                    {
                        isAuthenticated ?
                            <div>
                                <button
                                    className="mr-5 inline-flex items-center bg-gray-100 border-0 py-1 px-3 outline-dotted focus:outline-none hover:bg-gray-200 rounded text-base mt-4 md:mt-0"
                                    onClick={logOut}
                                >
                                    Logout
                                </button>
                            </div> :
                            <div>
                                <button
                                    className="mr-5 inline-flex items-center bg-gray-100 border-0 py-1 px-3 outline-dotted focus:outline-none hover:bg-gray-200 rounded text-base mt-4 md:mt-0">
                                    <Link href={"/register"}>Sign Up</Link>
                                </button>
                                <button
                                    className="mr-5 inline-flex items-center bg-gray-100 border-0 py-1 px-3 outline-dotted focus:outline-none hover:bg-gray-200 rounded text-base mt-4 md:mt-0">
                                    <Link href={"/login"}>Sign In
                                    </Link>
                                </button>
                            </div>
                    }
                </div>
            </header>
            <main className="flex flex-col p-24"> {children}
            </main>
        </Suspense>
        </body>
        </html>
    );
}
