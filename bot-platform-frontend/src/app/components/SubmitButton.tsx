export default function SubmitButton({text}: { text: string }) {
    return (<div>
        <button
            type="submit"
            className="flex w-full justify-center rounded-md bg-gray-100 px-3 py-1.5 outline-dotted font-semibold leading-6 text-base shadow-sm hover:bg-gray-200 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:bg-gray-200"
        >
            {text}
        </button>
    </div>)
}