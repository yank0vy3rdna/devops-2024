import {useState} from "react";

export default function TextInput({id, label, defaultText}: {
    id: string,
    label: string,
    defaultText?: string
}) {
    if (defaultText == undefined) {
        defaultText = ""
    }
    const [value, setValue] = useState(defaultText)
    const [valueSetted, setValueSetted] = useState(false)
    if (value == "" && defaultText != "" && !valueSetted) {
        setValue(defaultText)
        setValueSetted(true)
    }
    return (
        <div>
            <label htmlFor={id} className="block text-sm font-medium leading-6 text-gray-900">
                {label}
            </label>
            <div className="mt-2">
                <input
                    id={id}
                    name={id}
                    required
                    className="block w-full rounded-md border-0 p-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                    value={value}
                    onChange={(event) => {
                        setValue(event.currentTarget.value)
                    }}
                />
            </div>
        </div>
    )
}