"use client"
import useAuth from "@/app/hooks/auth";
import React, {FormEvent, useEffect, useState} from "react";
import axios from "axios";
import Link from "next/link";
import FormHeader from "@/app/components/FormHeader";
import TextInput from "@/app/components/TextInput";
import SubmitButton from "@/app/components/SubmitButton";


export default function Page() {
    const [token, isAuthenticated] = useAuth()
    const createBot = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault()
        axios.post("http://localhost:3001/bots", event.currentTarget, {
            headers: {
                Authorization: "Bearer " + token,
                'Content-Type': 'application/json',
            },
            validateStatus: null,
        }).then((response) => {
            if (response.status == 201) {
                window.location.replace("/bots")
            } else {
                alert("error while fetching bots")
            }
        })
    }
    return (<FormHeader text={"Create bot"}>
            <div className="mt-10 sm:mx-auto sm:w-full sm:max-w-sm">

                <form className="space-y-6" onSubmit={createBot}>
                    <TextInput id={"helloMessage"} label={"Текст, который будет отправлять бот"}/>
                    <TextInput id={"token"} label={"Токен из BotFather'а"}/>
                    <SubmitButton text={"Создать"}/>
                </form>
            </div>
        </FormHeader>
    )
}