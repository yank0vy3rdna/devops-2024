"use client"
import useAuth from "@/app/hooks/auth";
import React, {useEffect, useState} from "react";
import axios from "axios";
import Link from "next/link";


type Bot = {
    id: string
    token: string
    hello: string
    enabled: boolean
};
type BotsResponse = {
    Bots: Bot[]
}
export default function Page() {
    const [bots, setBots] = useState<Bot[]>([])
    const [token, isAuthenticated] = useAuth()
    useEffect(() => {
        if (token == "") {
            return
        }
        axios.get<BotsResponse>("http://localhost:3001/bots", {
            headers: {Authorization: "Bearer " + token},
            validateStatus: null,
        }).then((response) => {
            if (response.status == 200) {
                setBots(response.data.Bots)
            } else {
                alert("error while fetching bots")
            }
        })
    }, [token])
    return (<div className="flex items-center">
        {bots.length == 0 ?
            <div
                className="mr-20  py-1 px-3 rounded  text-base mt-4 md:mt-0">Пока
                что не создано ни одного бота
            </div> :
            <table className={"table-auto border-spacing-3 border-separate outline-dotted border"}>
                <thead className="bh-gray-300">
                <tr>
                    <th>ID</th>
                    <th>Text</th>
                    <th>Enabled</th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                {bots.map((bot) => <tr key={bot.id}>
                    <td>{bot.id}</td>
                    <td>{bot.hello}</td>
                    <td>{bot.enabled ? "Включен" : "Выключен"}</td>
                    <td>
                        <button
                            className="bg-gray-100 ml-14 border-0 py-1 px-3 outline-dotted focus:outline-none hover:bg-gray-200 rounded text-base md:mt-0">
                            <Link href={{pathname: "/bots/bot", query: {botId: bot?.id}}}>Редактировать</Link>
                        </button>
                    </td>
                </tr>)}
                </tbody>
            </table>
        }

        <button
            className="bg-gray-100 ml-14 h-14  border-0 py-1 px-3 outline-dotted focus:outline-none hover:bg-gray-200 rounded text-base md:mt-0">
            <Link href={"/bots/create"}>Создать</Link>
        </button>
    </div>)
}