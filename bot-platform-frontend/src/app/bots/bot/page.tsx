"use client"
import React, {useEffect, useState} from "react";
import useAuth from "@/app/hooks/auth";
import axios from "axios";
import Link from "next/link";
import {useSearchParams} from 'next/navigation'

type Bot = {
    id: string
    token: string
    hello: string
    enabled: boolean
};
export default function Page() {
    const params = useSearchParams()
    const botId = params.get("botId")

    const [bot, setBot] = useState<Bot>()
    const [token, isAuthenticated] = useAuth()
    const updateTable = () => {
        axios.get<Bot>("http://localhost:3001/bots/" + botId, {
            headers: {Authorization: "Bearer " + token},
            validateStatus: null,
        }).then((response) => {
            if (response.status == 200) {
                setBot(response.data)
            } else {
                alert("error while fetching bot")
            }
        })
    }
    useEffect(() => {
        if (token == "") {
            return
        }
        updateTable()
    }, [token, botId, updateTable])
    const changeAdmState = () => {
        axios.post("http://localhost:3001/bots/" + botId + "/changeAdministrativeState", {}, {
            headers: {Authorization: "Bearer " + token},
            validateStatus: null,
        }).then((response) => {
            if (response.status == 200) {
                updateTable()
            } else {
                alert("error while fetching bot")
            }
        })
    }
    const deleteBot = () => {
        axios.delete("http://localhost:3001/bots/" + botId, {
            headers: {Authorization: "Bearer " + token},
            validateStatus: null,
        }).then((response) => {
            if (response.status == 200) {
                window.location.replace("/bots")
            } else {
                alert("error while fetching bot")
            }
        })
    }
    return <div>
        <p>BotId: {botId}</p>
        <p> Token: {bot?.token}</p>
        <p> Message: {bot?.hello}</p>
        <p> Enabled: {bot?.enabled ? "Включен" : "Выключен"}</p>
        <div className="flex p-4">
            <button
                onClick={changeAdmState}
                className="bg-gray-100  m-2 mt-10 border-0 py-1 px-3 outline-dotted focus:outline-none hover:bg-gray-200 rounded text-base md:mt-0">
                {!bot?.enabled ? "Включить" : "Выключить"}
            </button>
            <button
                className="bg-gray-100  m-2 mt-10 border-0 py-1 px-3 outline-dotted focus:outline-none hover:bg-gray-200 rounded text-base md:mt-0">
                <Link href={{pathname: "/bots/bot/changeMessage", query: {botId: bot?.id}}}> Изменить сообщение</Link>
            </button>
            <button
                onClick={deleteBot}
                className="bg-gray-100  m-2 mt-10 border-0 py-1 px-3 outline-dotted focus:outline-none hover:bg-gray-200 rounded text-base md:mt-0">
                Удалить
            </button>
            <button
                className="bg-gray-100 m-2 mt-10 border-0 py-1 px-3 outline-dotted focus:outline-none hover:bg-gray-200 rounded text-base md:mt-0">
                <Link href={"/bots/"}>Назад</Link>
            </button>
        </div>
    </div>
}