"use client"
import useAuth from "@/app/hooks/auth";
import React, {FormEvent, useEffect, useState} from "react";
import axios from "axios";
import Link from "next/link";
import FormHeader from "@/app/components/FormHeader";
import TextInput from "@/app/components/TextInput";
import SubmitButton from "@/app/components/SubmitButton";
import {useSearchParams} from "next/navigation";


type Bot = {
    id: string
    token: string
    hello: string
    enabled: boolean
};
export default function Page() {
    const params = useSearchParams()
    const botId = params.get("botId")
    const [bot, setBot] = useState<Bot>()
    const [token] = useAuth()
    useEffect(() => {
        if (token == "") {
            return
        }
        axios.get<Bot>("http://localhost:3001/bots/" + botId, {
            headers: {Authorization: "Bearer " + token},
            validateStatus: null,
        }).then((response) => {
            if (response.status == 200) {
                setBot(response.data)
            } else {
                alert("error while fetching bot")
            }
        })
    }, [token, botId])
    const createBot = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault()
        axios.patch("http://localhost:3001/bots/" + botId, event.currentTarget, {
            headers: {
                Authorization: "Bearer " + token,
                'Content-Type': 'application/json',
            },
            validateStatus: null,
        }).then((response) => {
            if (response.status == 200) {
                window.location.replace("/bots/bot?botId=" + botId)
            } else {
                alert("error while fetching bots")
            }
        })
    }
    return (<FormHeader text={"Create bot"}>
            <div className="mt-10 sm:mx-auto sm:w-full sm:max-w-sm">

                <form className="space-y-6" onSubmit={createBot}>
                    <TextInput id={"helloMessage"} label={"Текст, который будет отправлять бот"}
                               defaultText={bot?.hello}/>
                    <SubmitButton text={"Сохранить"}/>
                    <button
                        className="flex w-full justify-center rounded-md bg-gray-100 px-3 py-1.5 outline-dotted font-semibold leading-6 text-base shadow-sm hover:bg-gray-200 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:bg-gray-200">
                        <Link href={{pathname: "/bots/bot", query: {botId: bot?.id}}}>Назад</Link>
                    </button>
                </form>
            </div>
        </FormHeader>
    )
}