"use client"
import {useEffect, useState} from "react";

export default function useAuth(): [string, boolean, (token: string) => void, () => void] {
    const [token, setToken] = useState("")
    console.log("rerender hook")
    const logOut = () => {
        localStorage.removeItem("token")
        localStorage.removeItem("token_exp")
    }
    useEffect(() => {
        const token = localStorage.getItem("token")
        const token_exp = localStorage.getItem("token_exp")
        if (token === null || token == "" || token_exp == null || Number(token_exp) <= Date.now() / 1000) {
            logOut()
        } else {
            setToken(token)
        }
    }, [])
    useEffect(() => {
        addEventListener("storage", (event) => {
            const token = localStorage.getItem("token")
            const token_exp = localStorage.getItem("token_exp")
            if (token === null || token == "" || token_exp == null || Number(token_exp) <= Date.now() / 1000) {
                logOut()
            } else {
                setToken(token)
            }
        });
    }, [])
    const changeToken = (token: string) => {
        const parseJwt = (token: string) => {
            const base64Url = token.split('.')[1];
            const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
            const jsonPayload = decodeURIComponent(window.atob(base64).split('').map(function (c) {
                return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
            }).join(''));
            return JSON.parse(jsonPayload);
        }
        localStorage.setItem("token", token)
        const exp = parseJwt(token)["exp"]
        localStorage.setItem("token_exp", exp)
        setToken(token)
    }

    return [token, token != "", changeToken, () => {
        logOut()
        window.location.replace("/login")
    }]
}
